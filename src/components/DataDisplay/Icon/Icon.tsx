'use client'
import IconWrapper from '@icon-park/react/es/all';
import {
  Fragment
} from 'react';
// import { IconProps } from './types/Icon.types';

export const Icon = (props: any) => {
  const {
    type,
    size,
    theme,
    fill,
    strokeWidth,
    className
  } = props

  return (
    <Fragment>
      <IconWrapper
        type={type}
        size={size}
        theme={theme}
        fill={fill}
        strokeWidth={strokeWidth}
        className={className}
      />
    </Fragment>
  )
}

