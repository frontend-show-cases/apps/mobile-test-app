'use client'
import { ProductCard } from '@/components';
import styles from './_css/catalogPage.module.scss';

const products = [
  {
    title: 'Title',
    price: {
      value: 12332,
      currency: 'USD'
    }
  },
  {
    title: 'Title',
    price: {
      value: 12332,
      currency: 'USD'
    }
  },
  {
    title: 'Title',
    price: {
      value: 12332,
      currency: 'USD'
    }
  },
  {
    title: 'Title',
    price: {
      value: 12332,
      currency: 'USD'
    }
  },
  {
    title: 'Title',
    price: {
      value: 12332,
      currency: 'USD'
    }
  },
  {
    title: 'Title',
    price: {
      value: 12332,
      currency: 'USD'
    }
  },
  {
    title: 'Title',
    price: {
      value: 12332,
      currency: 'USD'
    }
  },
  {
    title: 'Title',
    price: {
      value: 12332,
      currency: 'USD'
    }
  },
  {
    title: 'Title',
    price: {
      value: 12332,
      currency: 'USD'
    }
  },
  {
    title: 'Title',
    price: {
      value: 12332,
      currency: 'USD'
    }
  },
  {
    title: 'Title',
    price: {
      value: 12332,
      currency: 'USD'
    }
  }
]

export default function CatalogScreen() {
  const items = products.map((item: any, i: number) => {
    return <ProductCard key={i} />
  })
  return (
    <div className={styles.page}>
      {items}
    </div>
  )
}