'use client'
import { useEffect, useState } from 'react';
import { useRouter, usePathname } from 'next/navigation';
import { tabBarItems } from '@/variables/navigation/bottomNavigationBar';
import { TabBarItem } from './TabBarItem';
import styles from './TabBar.module.scss';

export const TabBar = () => {
  const [activeRoute, setActiveRoute] = useState('')
  const router = useRouter();
  const path = usePathname();
  console.log('path', path.split('/')[1])

  useEffect(() => {
    const activeTab = tabBarItems.filter(tab => tab.path === path)[0]
    setActiveRoute(!!activeTab ? activeTab.id : '')
  }, [path])

  // console.log(activeRoute)

  const width = 100 / tabBarItems.length

  const tabs = tabBarItems.map((tab: any) => {
    return (
      <TabBarItem
        key={tab.id}
        {...tab}
        isActive={activeRoute === tab.id}
        width={width}
      />
    )
  })

  return (
    <div className={styles.tab_bar}>
      {tabs}
    </div>
      // <TabBar
      //   className={styles.nav_bar}
      //   safeArea
      //   onChange={tabKey => qwe(tabKey)}
      // >
      //   {tabs.map((item: any) => (
      //     <TabBar.Item
      //       key={item.key}
      //       icon={<Icon
      //         type={item.icon}
      //         size={25}
      //         theme="filled"
      //       />}
      //       title={item.title}
      //       badge={item.badge}
      //     />
      //   ))}
      // </TabBar>
  )
}