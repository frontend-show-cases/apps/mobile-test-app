export const tabBarItems = [
  {
    id: 'catalog',
    path: '/catalog',
    title: 'Catalog',
    icon: 'System',
    badge: '5',
  },
  {
    id: 'qrscanner',
    path: '/catalog/qrscanner',
    title: 'QR scanner',
    icon: 'ScanningTwo',
    badge: '5',
  },
  {
    id: 'profile',
    path: '/profile',
    title: 'Profile',
    icon: 'User',
    badge: '99+',
  },
]