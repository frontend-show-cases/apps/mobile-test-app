'use client'
import { useState } from 'react';
import { useRouter } from 'next/navigation';
import { Badge } from 'antd-mobile';
import { Icon } from "@/components";
import styles from './TabBarItem.module.scss';


export const TabBarItem = (props: any) => {
  const {
    path,
    title,
    icon,
    badge,
    isActive,
    width
  } = props;

  const router = useRouter();
  console.log(title, isActive)

  return (
    <div
      onClick={() => router.push(path)}
      className={styles.tab_bar__item_wrapper}
      style={{width: `${width}%`, color: `${isActive ? 'green' : 'red'}`}}
    >
      <div className={styles.tab_bar__item}>
        <Badge content={badge} style={{ '--top': '10%' }}>
          <Icon
            type={icon}
            size={22}
            theme="filled"
            fill={['#7E7E7E' ,'red']}
          />
        </Badge>
        <div className={styles.tab_bar__item_title}>{title}</div>
      </div>
    </div>
  )
}
