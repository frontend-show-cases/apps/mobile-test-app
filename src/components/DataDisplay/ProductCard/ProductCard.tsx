'use client'
import { useRouter } from 'next/navigation';
import { Image } from 'antd-mobile';
import styles from './ProductCard.module.scss';

const demoSrc = 'https://static.nike.com/a/images/t_PDP_1280_v1/f_auto,q_auto:eco,u_126ab356-44d8-4a06-89b4-fcdcc8df0245,c_scale,fl_relative,w_1.0,h_1.0,fl_layer_apply/ded10b30-74d9-4d8c-931e-54a5911654eb/air-jordan-1-low-shoes-459b4T.png'

export const ProductCard = () => {
  const router = useRouter()
  return (
    <div
      className={styles.product_card}
      onClick={() => router.push('/catalog/asd')}
    >
      <div className={styles.product_card__image_wrapper}>
        <Image
          className={styles.product_card__image}
          src={demoSrc}
          width="100%"
          height="100%"
          fit='cover'
        />
      </div>
      <div className={styles.product_card__details}>
        <div className={styles.product_card__title}>Product title</div>
        <div className={styles.product_card__price}>$ 3500</div>
        {/* <div className={styles.product_card__description}>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.</div> */}
      </div>
    </div>
  )
}