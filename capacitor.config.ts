import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.example.app',
  appName: 'mobile-test-app',
  webDir: 'out',
	bundledWebRuntime: false,
	server: {
		url: 'http://192.168.1.4:3000',
		cleartext: true
	}
};

export default config;
